var express = require("express");
var bodyParser = require("body-parser");
var session = require("express-session");
var routes = require("./routes");
var config = require("./config");
var app = express();

app.use(bodyParser.json());

// Set up session
app.use(session({
    // genid: function(){ /* write your id generator here */}, //optional
    // name: 'myCookie',
    secret: "1111",                 // secret/salt used to hash session ID
    resave: false,                  // if true, forces saving to store even if not modified
    saveUninitialized: true,        // forces new sessions to be saved to the store
    cookie: { secure: true },      // cookie configuration
}));



// creates cart if non existent
app.use(function (req, res, next) {
    if (!req.session.cart) {
        req.session.name = "mycart";
        req.session.cart = [];
    }
    next();
})

// ROUTES SET UP ----------------------
routes.init(app);
routes.errorHandler(app);


// SERVER / PORT SETUP ------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(config.express.port, config.express.ip, function () {
    console.log('Server running at http://%s:%s', config.express.ip, config.express.port);
});