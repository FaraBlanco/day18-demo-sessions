// DEPENDENCIES ------------------------
// Third Party -------------------------
var express = require('express');
var path = require('path');

// CONSTANTS --------------------------
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');


// EXPOSED FUNCTIONS ------------------
module.exports = {
    init: configureRoutes,                   
    errorHandler: errorHandler
}

// Function definition
function configureRoutes(app) {

    app.get("/api/cart", function (req, res) {
        console.log("Get Cart");
        console.log("Req Session -> " + JSON.stringify(req.session));
        console.log("session ID" + req.session.id);
        res.status(200).json(req.session.cart);
    });

    app.post("/api/cart", function (req, res) {
        req.session.cart.push(req.body.item);
        console.log("Add to Cart");
        console.log("Req Session -> " + JSON.stringify(req.session));
        console.log("session ID" + req.session.id)
        res.status(202).end();
    })

    app.delete("/api/cart", function (req, res) {
        //Save cart to database first before destroying
        console.log("session ID" + req.session.id)
        req.session.destroy();
        res.status(200).end();
    });

    app.use(express.static(CLIENT_FOLDER));
};

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(MSG_FOLDER + "/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(MSG_FOLDER + '/500.html'));
    });
};