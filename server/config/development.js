// Express ----------------------------
const express = {
  port: process.env.PORT || process.argv[2] || 8080,
  ip: '127.0.0.1',
};

// Database ---------------------------
const mongodb = {
  uri: process.env.MONGODB_URI || 'mongodb://localhost:27017/myproject-dev',
};

// Exposed configurations -------------
module.exports = {
    express: express,                   
    mongodb: mongodb,
}

